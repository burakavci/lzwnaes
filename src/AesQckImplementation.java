import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

//this implementation takes cipher text, uses it's md5 hash
public class AesQckImplementation {
    public static final int[] sbox = {0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, 0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16};
    public static final byte[][] mixColumnMatrix = {{0x02, 0x03, 0x01, 0x01}, {0x01, 0x02, 0x03, 0x01}, {0x01, 0x01, 0x02, 0x03}, {0x03, 0x01, 0x01, 0x02}};
    public static final byte[][] invMixColumnMatrix = {{0x0E, 0x0b, 0x0d, 0x09}, {0x09, 0x0e, 0x0b, 0x0d}, {0x0d, 0x09, 0x0e, 0x0b}, {0x0b, 0x0d, 0x09, 0x0e}};
    public static final byte[] rcon = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, (byte) 0x80, 0x1b, 0x36};
    private static final int[] invSbox = {0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81,
            0xf3, 0xd7, 0xfb, 0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9,
            0xcb, 0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e, 0x08,
            0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25, 0x72, 0xf8, 0xf6,
            0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, 0x6c, 0x70, 0x48, 0x50, 0xfd,
            0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84, 0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3,
            0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06, 0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1,
            0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b, 0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf,
            0xce, 0xf0, 0xb4, 0xe6, 0x73, 0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c,
            0x75, 0xdf, 0x6e, 0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe,
            0x1b, 0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4, 0x1f,
            0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f, 0x60, 0x51, 0x7f,
            0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, 0xa0, 0xe0, 0x3b, 0x4d, 0xae,
            0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61, 0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6,
            0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d};

    public static byte[] encrypt(byte[] data, byte[] cipher) throws NoSuchAlgorithmException {
        data = copyAndPadData(data);
        cipher = MessageDigest.getInstance("MD5").digest(cipher);
        byte[][] rkey = byteArrayTo4x4Matrix(cipher);
        byte[][][] dataDivided = new byte[data.length / 16][4][4];
        for (int i = 0; i < dataDivided.length; i++) {
            for (int y = 0; y < 4; y++) {
                for (int x = 0; x < 4; x++) {
                    dataDivided[i][y][x] = data[i * 16 + y * 4 + x];
                }
            }
        }
        byte[] encryptedData = new byte[data.length];
        for (int i = 0; i < dataDivided.length; i++) {
            encrypt4x4Matrix(dataDivided[i], rkey);
            System.arraycopy(_4x4MatrixToByteArray(dataDivided[i]), 0, encryptedData, i * 16, 16);
        }

        return encryptedData;
    }

    public static void encrypt4x4Matrix(byte[][] data, byte[][] cipher) {
        XorRoundKey(data, cipher);
        for (int i = 1; i < 10; i++) {
            Round(data, cipher, i);
        }
        SubBytes(data);
        ShiftRows(data);
        XorRoundKey(data, GetRoundKey(cipher,10));
    }

    public static void Round(byte[][] data, byte[][] cipher, int round) {
        SubBytes(data);
        ShiftRows(data);
        MixColumns(data);
        XorRoundKey(data, GetRoundKey(cipher,round));
    }

    public static void decrypt4x4Matrix(byte[][] data, byte[][] cipher) {
        XorRoundKey(data, GetRoundKey(cipher, 10));
        InverseSubBytes(data);
        InverseShiftRows(data);
        for (int i = 9; i >= 1; i--) {
            InverseRound(data, cipher, i);
        }
        XorRoundKey(data, cipher);
    }

    public static void InverseRound(byte[][] data, byte[][] cipher, int round) {
        XorRoundKey(data, GetRoundKey(cipher, round));
        InverseMixColumns(data);
        InverseSubBytes(data);
        InverseShiftRows(data);
    }

    public static byte[][] GetRoundKey(byte[][] rKey, int round) {
        byte[][] temp = new byte[rKey.length][rKey[0].length];
        for (int i = 0; i < rKey.length; i++) {
            System.arraycopy(rKey[i], 0, temp[i], 0, rKey[i].length);
        }
        for (int i = 1; i <= round; i++) {
            RoundKey(temp, i);
        }
        return temp;
    }

    public static void RoundKey(byte[][] rKey, int round) {
        byte[] rotatedCol = {rKey[1][3], rKey[2][3], rKey[3][3], rKey[0][3]};
        byte[] newKeyFirstCol = {rcon[round - 1], 0x0, 0x0, 0x0};
        for (int i = 0; i < 4; i++) {
            rotatedCol[i] = (byte) sbox[rotatedCol[i] & 0xFF];
            rKey[i][0] = (byte) (newKeyFirstCol[i] ^ rotatedCol[i] ^ rKey[i][0]);
        }
        for (int i = 1; i < 4; i++)//yatay hareket - sütun değişimi
        {
            for (int j = 0; j < 4; j++)//dikey hareket - satır değişimi
            {
                rKey[j][i] = (byte) (rKey[j][i] ^ rKey[j][i - 1]);
            }
        }
    }

    public static void XorRoundKey(byte[][] data, byte[][] cipher) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                data[i][j] ^= cipher[i][j];
            }
        }
    }

    public static void MixColumns(byte[][] data) {
        byte[][] result = multiplyMatrices(mixColumnMatrix, data);
        for (int i = 0; i < 4; i++) {
            System.arraycopy(result[i], 0, data[i], 0, 4);
        }
    }

    public static void SubBytes(byte[][] data) {
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                data[y][x] = (byte) sbox[data[y][x] & 0xFF];
            }
        }
    }

    public static void ShiftRows(byte[][] data) {
        //ex input: 0x23, 0x33, 0xFF, 0x15
        //ex output: 0x33, 0xFF, 0x15, 0x23
        byte temp = data[1][0];
        data[1][0] = data[1][1];
        data[1][1] = data[1][2];
        data[1][2] = data[1][3];
        data[1][3] = temp;

        //ex input: 0x23, 0x33, 0xFF, 0x15
        //ex output: 0xFF, 0x15, 0x23, 0x33
        temp = data[2][0];
        data[2][0] = data[2][2];
        data[2][2] = temp;
        temp = data[2][1];
        data[2][1] = data[2][3];
        data[2][3] = temp;

        //ex input: 0x23, 0x33, 0xFF, 0x15
        //ex output: 0x15, 0x23, 0x33, 0xFF
        temp = data[3][0];
        data[3][0] = data[3][3];
        data[3][3] = data[3][2];
        data[3][2] = data[3][1];
        data[3][1] = temp;
    }

    public static void InverseShiftRows(byte[][] data) {
        //ex input: 0x33, 0xFF, 0x15, 0x23
        //ex output: 0x23, 0x33, 0xFF, 0x15
        byte temp = data[1][0];
        data[1][0] = data[1][3];
        data[1][3] = data[1][2];
        data[1][2] = data[1][1];
        data[1][1] = temp;

        //ex input: 0xFF, 0x15, 0x23, 0x33
        //ex output: 0x23, 0x33, 0xFF, 0x15
        temp = data[2][0];
        data[2][0] = data[2][2];
        data[2][2] = temp;
        temp = data[2][1];
        data[2][1] = data[2][3];
        data[2][3] = temp;

        //ex input: 0x15, 0x23, 0x33, 0xFF
        //ex output: 0x23, 0x33, 0xFF, 0x15
        temp = data[3][0];
        data[3][0] = data[3][1];
        data[3][1] = data[3][2];
        data[3][2] = data[3][3];
        data[3][3] = temp;
    }

    public static void InverseSubBytes(byte[][] data) {
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                data[y][x] = (byte) invSbox[data[y][x] & 0xFF];
            }
        }
    }

    public static void InverseMixColumns(byte[][] data) {
        byte[][] result = multiplyMatrices(invMixColumnMatrix, data);
        for (int i = 0; i < 4; i++) {
            System.arraycopy(result[i], 0, data[i], 0, 4);
        }
    }

    public static byte[][] byteArrayTo4x4Matrix(byte[] arr) {
        byte[] paddedArr;
        if (arr.length % 16 != 0) {
            paddedArr = copyAndPadData(arr);
        } else {
            paddedArr = arr;
        }
        byte[][] matrix = new byte[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = paddedArr[i * 4 + j];
            }
        }
        return matrix;
    }

    public static byte[] _4x4MatrixToByteArray(byte[][] arr) {
        byte[] result = new byte[1];
        int index;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                index = i * arr.length + j;
                if (result.length < index + 1) {
                    result = Arrays.copyOf(result, result.length + 1);
                }
                result[index] = arr[i][j];
            }
        }
        return result;
    }

    public static byte[] copyAndPadData(byte[] data) {
        int dataModulo = data.length % 16;
        int dataCountToFill = (16 - dataModulo) % 16;
        byte[] resizedData = new byte[data.length + dataCountToFill];
        java.lang.System.arraycopy(data, 0, resizedData, 0, data.length);
        return resizedData;
    }

    public static byte[] removePadding(byte[] data) {
        int dataCountToTrim = 0;
        for (int i = data.length - 1; i > -1; i--)
        {
            if(data[i] == 0x00)
                dataCountToTrim++;
            else
            {
                break;
            }
        }
        byte[] resizedData = new byte[data.length - dataCountToTrim];
        java.lang.System.arraycopy(data, 0, resizedData, 0, data.length - dataCountToTrim);
        return resizedData;
    }

    public static byte[][] multiplyMatrices(byte[][] firstMatrix, byte[][] secondMatrix) {
        byte[][] result = new byte[firstMatrix.length][secondMatrix[0].length];

        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                result[row][col] = multiplyMatricesCellOverGaiosField(firstMatrix, secondMatrix, row, col);
            }
        }

        return result;
    }

    public static byte multiplyMatricesCellOverGaiosField(byte[][] firstMatrix, byte[][] secondMatrix, int row, int col) {
        byte cell = 0;
        int result;
        for (int i = 0; i < secondMatrix.length; i++) {
            result = gsMultiply(firstMatrix[row][i], secondMatrix[i][col]);
            cell ^= result;
        }
        return cell;
    }

    public static byte gsMultiply(byte a, byte b) {
        byte returnValue = 0;
        byte temp = 0;
        while (a != 0) {
            if ((a & 1) != 0)
                returnValue = (byte) (returnValue ^ b);
            temp = (byte) (b & 0x80);
            b = (byte) (b << 1);
            if (temp != 0)
                b = (byte) (b ^ 0x1b);
            a = (byte) ((a & 0xff) >> 1);
        }
        return returnValue;
    }


    public static byte[] decrypt(byte[] data, byte[] cipher) throws NoSuchAlgorithmException {
        cipher = MessageDigest.getInstance("MD5").digest(cipher);
        byte[][] rkey = byteArrayTo4x4Matrix(cipher);
        byte[][][] dataDivided = new byte[data.length / 16][4][4];
        for (int i = 0; i < dataDivided.length; i++) {
            for (int y = 0; y < 4; y++) {
                for (int x = 0; x < 4; x++) {
                    dataDivided[i][y][x] = data[i * 16 + y * 4 + x];
                }
            }
        }
        byte[] decryptedData = new byte[data.length];
        for (int i = 0; i < dataDivided.length; i++) {
            decrypt4x4Matrix(dataDivided[i], rkey);
            System.arraycopy(_4x4MatrixToByteArray(dataDivided[i]), 0, decryptedData, i * 16, 16);
        }

        return removePadding(decryptedData);
    }
}
