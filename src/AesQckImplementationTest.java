import org.junit.jupiter.api.Assertions;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

class AesQckImplementationTest {

    @org.junit.jupiter.api.Test
    void copyAndPadDataTest() {
        byte[] testData = new byte[18];
        byte[] testCipher = new byte[1];
        Random rGen = new Random(1);
        rGen.nextBytes(testData); //böyle bişey mi varmış
        byte[] result = AesQckImplementation.copyAndPadData(testData);
        System.out.println(bytesToHex(result));
        Assertions.assertEquals(Math.ceil(testData.length / 16.0d) * 16, result.length);

    }

    public static String bytesToHex(byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for (byte b : in) {
            builder.append("0x");
            builder.append(String.format("%02x", b));
            builder.append(", ");
        }
        builder.setLength(builder.length() - 2);
        return builder.toString();
    }

    @org.junit.jupiter.api.Test
    void decrypt() {
    }

    @org.junit.jupiter.api.Test
    void ShiftRowsTest() {
        byte[][] testData = new byte[4][4];
        for (int i = 0; i < 4; i++) {
            testData[i] = new byte[]{0x23, 0x33, (byte) 0xFF, 0x15};
        }
        byte[][] expectedResult = new byte[][]{{0x23, 0x33, (byte) 0xFF, 0x15}, {0x33, (byte) 0xFF, 0x15, 0x23}, {(byte) 0xFF, 0x15, 0x23, 0x33}, {0x15, 0x23, 0x33, (byte) 0xFF}};
        AesQckImplementation.ShiftRows(testData);
        for (int i = 0; i < 4; i++) {
            System.out.println("testData[" + i + "]       = " + bytesToHex(testData[i]));
            System.out.println("expectedResult[" + i + "] = " + bytesToHex(expectedResult[i]));
            System.out.println();
            for (int j = 0; j < 4; j++) {
                Assertions.assertEquals(expectedResult[i][j], testData[i][j]);
            }
        }
    }

    @org.junit.jupiter.api.Test
    void InverseShiftRowsTest() {
        byte[][] testData = new byte[][]{{0x23, 0x33, (byte) 0xFF, 0x15}, {0x33, (byte) 0xFF, 0x15, 0x23}, {(byte) 0xFF, 0x15, 0x23, 0x33}, {0x15, 0x23, 0x33, (byte) 0xFF}};
        byte[][] expected = new byte[4][4];
        for (int i = 0; i < 4; i++) {
            expected[i] = new byte[]{0x23, 0x33, (byte) 0xFF, 0x15};
        }
        AesQckImplementation.InverseShiftRows(testData);
        for (int i = 0; i < 4; i++) {
            System.out.println("testData[" + i + "]       = " + bytesToHex(testData[i]));
            System.out.println("expectedResult[" + i + "] = " + bytesToHex(expected[i]));
            System.out.println();
            for (int j = 0; j < 4; j++) {
                Assertions.assertEquals(expected[i][j], testData[i][j]);
            }
        }
    }

    @org.junit.jupiter.api.Test
    void MultiplyWithMixColumnMatrixSingleColumnTest() {
        byte[][] testData = {{(byte) 0xd4}, {(byte) 0xbf}, {0x5d}, {0x30}};
        byte[][] expected = {{0x04}, {0x66}, {(byte) 0x81}, {(byte) 0xe5}};
        byte[][] result = AesQckImplementation.multiplyMatrices(AesQckImplementation.mixColumnMatrix, testData);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "][0] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "][0]   = " + bytesToHex(result[i]));
            System.out.println();
            Assertions.assertEquals(expected[i][0], result[i][0]);
        }
    }

    @org.junit.jupiter.api.Test
    void MultiplyWithMixColumnMatrixTest() {
        byte[][] testData = {{0x49, 0x45, 0x7f, 0x77}, {(byte) 0xdb, 0x39, 0x02, (byte) 0xde}, {(byte) 0x87, 0x53, (byte) 0xd2, (byte) 0x96}, {0x3b, (byte) 0x89, (byte) 0xf1, 0x1a}};
        byte[][] expected = {{0x58, 0x1b, (byte) 0xdb, 0x1b}, {0x4d, 0x4b, (byte) 0xe7, 0x6b}, {(byte) 0xca, 0x5a, (byte) 0xca, (byte) 0xb0}, {(byte) 0xf1, (byte) 0xac, (byte) 0xa8, (byte) 0xe5}};
        byte[][] result = AesQckImplementation.multiplyMatrices(AesQckImplementation.mixColumnMatrix, testData);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "]   = " + bytesToHex(result[i]));
            System.out.println();
            Assertions.assertArrayEquals(expected[i], result[i]);
        }
    }

    @org.junit.jupiter.api.Test
    void MixColumnsTest() {
        byte[][] testData = {{0x49, 0x45, 0x7f, 0x77}, {(byte) 0xdb, 0x39, 0x02, (byte) 0xde}, {(byte) 0x87, 0x53, (byte) 0xd2, (byte) 0x96}, {0x3b, (byte) 0x89, (byte) 0xf1, 0x1a}};
        byte[][] expected = {{0x58, 0x1b, (byte) 0xdb, 0x1b}, {0x4d, 0x4b, (byte) 0xe7, 0x6b}, {(byte) 0xca, 0x5a, (byte) 0xca, (byte) 0xb0}, {(byte) 0xf1, (byte) 0xac, (byte) 0xa8, (byte) 0xe5}};
        AesQckImplementation.MixColumns(testData);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "]   = " + bytesToHex(testData[i]));
            System.out.println();
            Assertions.assertArrayEquals(expected[i], testData[i]);
        }
    }

    @org.junit.jupiter.api.Test
    void RoundTest() {
        byte[][] testData = intMatrixToByteMatrix(new int[][]{{0x00, 0x3c, 0x6e, 0x47}, {0x1f, 0x4e, 0x22, 0x74}, {0x0e, 0x08, 0x1b, 0x31}, {0x54, 0x59, 0x0b, 0x1a}});
        byte[][] testCipher = intMatrixToByteMatrix(new int[][]{{0x54, 0x73, 0x20, 0x67}, {0x68, 0x20, 0x4b, 0x20}, {0x61, 0x6d, 0x75, 0x46}, {0x74, 0x79, 0x6e, 0x75}});
        byte[][] expected = intMatrixToByteMatrix(new int[][]{{0x58, 0x15, 0x59, 0xcd}, {0x47, 0xb6, 0xd4, 0x39}, {0x08, 0x1c, 0xe2, 0xdf}, {0x8b, 0xba, 0xe8, 0xce}});
        AesQckImplementation.Round(testData, testCipher, 1);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "]   = " + bytesToHex(testData[i]));
            System.out.println();
            Assertions.assertArrayEquals(expected[i], testData[i]);
        }
    }

    @org.junit.jupiter.api.Test
    void RoundKeyTest() {
        byte[][] testData = intMatrixToByteMatrix(new int[][]{{0x2b, 0x28, 0xab, 0x09}, {0x7e, 0xae, 0xf7, 0xcf}, {0x15, 0xd2, 0x15, 0x4f}, {0x16, 0xa6, 0x88, 0x3c}});
        byte[][] expected = intMatrixToByteMatrix(new int[][]{{0xa0, 0x88, 0x23, 0x2a}, {0xfa, 0x54, 0xa3, 0x6c}, {0xfe, 0x2c, 0x39, 0x76}, {0x17, 0xb1, 0x39, 0x05}});
        AesQckImplementation.RoundKey(testData, 1);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "]   = " + bytesToHex(testData[i]));
            System.out.println();
            Assertions.assertArrayEquals(expected[i], testData[i]);
        }
    }

    @org.junit.jupiter.api.Test
    void GetRoundKeyTest() {
        byte[][] testData = intMatrixToByteMatrix(new int[][]{{0x2b, 0x28, 0xab, 0x09}, {0x7e, 0xae, 0xf7, 0xcf}, {0x15, 0xd2, 0x15, 0x4f}, {0x16, 0xa6, 0x88, 0x3c}});
        byte[][] expected = intMatrixToByteMatrix(new int[][]{{0xa0, 0x88, 0x23, 0x2a}, {0xfa, 0x54, 0xa3, 0x6c}, {0xfe, 0x2c, 0x39, 0x76}, {0x17, 0xb1, 0x39, 0x05}});
        byte[][] result = AesQckImplementation.GetRoundKey(testData, 1);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "]   = " + bytesToHex(result[i]));
            System.out.println();
            Assertions.assertArrayEquals(expected[i], result[i]);
        }
    }

    @org.junit.jupiter.api.Test
    void encrypt4x4MatrixTest() {
        byte[][] testData = intMatrixToByteMatrix(new int[][]{{0x54, 0x4f, 0x4e, 0x20}, {0x77, 0x6e, 0x69, 0x54}, {0x6f, 0x65, 0x6e, 0x77}, {0x20, 0x20, 0x65, 0x6f}});
        byte[][] testCipher = intMatrixToByteMatrix(new int[][]{{0x54, 0x73, 0x20, 0x67}, {0x68, 0x20, 0x4b, 0x20}, {0x61, 0x6d, 0x75, 0x46}, {0x74, 0x79, 0x6e, 0x75}});
        byte[][] expected = intMatrixToByteMatrix(new int[][]{{0x29, 0x57, 0x40, 0x1a}, {0xc3, 0x14, 0x22, 0x02}, {0x50, 0x20, 0x99, 0xd7}, {0x5f, 0xf6, 0xb3, 0x3a}});
        AesQckImplementation.encrypt4x4Matrix(testData, testCipher);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "]   = " + bytesToHex(testData[i]));
            System.out.println();
            Assertions.assertArrayEquals(expected[i], testData[i]);
        }
    }

    @org.junit.jupiter.api.Test
    void md5EncryptDecryptTest() throws NoSuchAlgorithmException {
        byte[] testData = {0x54, 0x4f, 0x4e, 0x20, 0x77, 0x6e, 0x69, 0x54, 0x6f, 0x65, 0x6e, 0x77, 0x20, 0x20, 0x65, 0x6f};
        byte[] testCipher = {0x54, 0x73, 0x20, 0x67, 0x68, 0x20, 0x4b, 0x20, 0x61, 0x6d, 0x75, 0x46, 0x74, 0x79, 0x6e, 0x75};
        byte[][] matrix = AesQckImplementation.byteArrayTo4x4Matrix(testData);
        AesQckImplementation.encrypt4x4Matrix(matrix, AesQckImplementation.byteArrayTo4x4Matrix(MessageDigest.getInstance("MD5").digest(testCipher)));
        AesQckImplementation.decrypt4x4Matrix(matrix,AesQckImplementation.byteArrayTo4x4Matrix(MessageDigest.getInstance("MD5").digest(testCipher)));
        System.out.println("expected = " + bytesToHex(testData));
        System.out.println("result   = " + bytesToHex(AesQckImplementation._4x4MatrixToByteArray(matrix)));
        System.out.println();
        Assertions.assertArrayEquals(testData, AesQckImplementation._4x4MatrixToByteArray(matrix));
    }

    @org.junit.jupiter.api.Test
    void encryptDecryptTest() throws NoSuchAlgorithmException {
        byte[] testData = {0x54, 0x4f, 0x4e, 0x20, 0x77, 0x6e, 0x69, 0x54, 0x6f, 0x65, 0x6e, 0x77, 0x20, 0x20, 0x65, 0x6f};
        byte[] testCipher = {0x54, 0x73, 0x20, 0x67, 0x68, 0x20, 0x4b, 0x20, 0x61, 0x6d, 0x75, 0x46, 0x74, 0x79, 0x6e, 0x75};
        byte[] encrypted = AesQckImplementation.encrypt(testData,testCipher);
        byte[] decrypted = AesQckImplementation.decrypt(encrypted, testCipher);
        System.out.println("expected = " + bytesToHex(testData));
        System.out.println("result   = " + bytesToHex(decrypted));
        System.out.println();
        Assertions.assertArrayEquals(testData, decrypted);
    }

    @org.junit.jupiter.api.Test
    void encryptDecryptTestWithText() throws NoSuchAlgorithmException {
        String testText = "I Love Linux";
        String testCipher = "This is a test cipher";
        byte[] encrypted = AesQckImplementation.encrypt(testText.getBytes(),testCipher.getBytes());
        byte[] decrypted = AesQckImplementation.decrypt(encrypted, testCipher.getBytes());
        String result = new String(decrypted);
        System.out.println("expected = " + testText);
        System.out.println("result   = " + result);
        System.out.println();
        Assertions.assertEquals(testText, result);
    }

    @org.junit.jupiter.api.Test
    void encryptTest() throws NoSuchAlgorithmException {
        byte[] testData = {0x54, 0x4f, 0x4e, 0x20, 0x77, 0x6e, 0x69, 0x54, 0x6f, 0x65, 0x6e, 0x77, 0x20, 0x20, 0x65, 0x6f};
        byte[] testCipher = {0x54, 0x73, 0x20, 0x67, 0x68, 0x20, 0x4b, 0x20, 0x61, 0x6d, 0x75, 0x46, 0x74, 0x79, 0x6e, 0x75};
        byte[][] matrix = AesQckImplementation.byteArrayTo4x4Matrix(testData);
        AesQckImplementation.encrypt4x4Matrix(matrix, AesQckImplementation.byteArrayTo4x4Matrix(MessageDigest.getInstance("MD5").digest(testCipher)));
        byte[] result = AesQckImplementation.encrypt(testData, testCipher);
        System.out.println("expected   = " + bytesToHex(AesQckImplementation._4x4MatrixToByteArray(matrix)));
        System.out.println("result = " + bytesToHex(result));
        System.out.println();
        Assertions.assertArrayEquals(AesQckImplementation._4x4MatrixToByteArray(matrix), result);
    }

    @org.junit.jupiter.api.Test
    void MultiplyWithInverseMixColumnMatrixSingleColumnTest() {
        byte[][] testData = {{0x04}, {0x66}, {(byte) 0x81}, {(byte) 0xe5}};
        byte[][] expected = {{(byte) 0xd4}, {(byte) 0xbf}, {0x5d}, {0x30}};
        byte[][] result = AesQckImplementation.multiplyMatrices(AesQckImplementation.invMixColumnMatrix, testData);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "][0] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "][0]   = " + bytesToHex(result[i]));
            System.out.println();
            Assertions.assertEquals(expected[i][0], result[i][0]);
        }
    }

    @org.junit.jupiter.api.Test
    void decrypt4x4MatrixTest() {
        byte[][] testData = intMatrixToByteMatrix(new int[][]{{0x29, 0x57, 0x40, 0x1a}, {0xc3, 0x14, 0x22, 0x02}, {0x50, 0x20, 0x99, 0xd7}, {0x5f, 0xf6, 0xb3, 0x3a}});
        byte[][] expected = intMatrixToByteMatrix(new int[][]{{0x54, 0x4f, 0x4e, 0x20}, {0x77, 0x6e, 0x69, 0x54}, {0x6f, 0x65, 0x6e, 0x77}, {0x20, 0x20, 0x65, 0x6f}});
        byte[][] testCipher = intMatrixToByteMatrix(new int[][]{{0x54, 0x73, 0x20, 0x67}, {0x68, 0x20, 0x4b, 0x20}, {0x61, 0x6d, 0x75, 0x46}, {0x74, 0x79, 0x6e, 0x75}});
        AesQckImplementation.decrypt4x4Matrix(testData, testCipher);
        for (int i = 0; i < 4; i++) {
            System.out.println("expected[" + i + "] = " + bytesToHex(expected[i]));
            System.out.println("result[" + i + "]   = " + bytesToHex(testData[i]));
            System.out.println();
            Assertions.assertArrayEquals(expected[i], testData[i]);
        }
    }

    byte[][] intMatrixToByteMatrix(int[][] matrix) {
        byte[][] arr = new byte[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                arr[i][j] = (byte) matrix[i][j];
            }
        }
        return arr;
    }
}
