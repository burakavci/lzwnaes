This project was done purely for fun (and college grades). It's not recommended to use it in a production environment.

# **AES**

## **Usage**

You can use the library by simply passing the byte array representations of strings. 

**Simple Example**

```java
String text = "I Love Linux";
String pass = "This is a test cipher";
byte[] encrypted = AesQckImplementation.encrypt(text.getBytes(),pass.getBytes());
byte[] decrypted = AesQckImplementation.decrypt(encrypted, pass.getBytes());
```

# **LZW**

## **Usage**

**Simple Example**

```java
String testText = "Compression_Compressed_Compress_Comp";
ArrayList<Integer> compressed = LzwQckImplementation.compress(testText);
String decompressed = LzwQckImplementation.decompress(compressed);
double compressRatio = (1 - compressed.size() / (double) (testText.length()));
double compressedBy = compressRatio * 100;
System.out.println("Compress ratio: " + compressRatio);
System.out.println("Compressed by: " + compressedBy + " %");
```

