import org.junit.jupiter.api.Assertions;

import java.util.Random;

public class QckCompressorTest {

    @org.junit.jupiter.api.Test
    void compressTest() {
        Random r = new Random();
        byte[] testData = new byte[512];
        r.nextBytes(testData);
        byte[] compressed = QckCompressor.compress(testData);
        byte[] decompressed = QckCompressor.decompress(compressed);
        Assertions.assertArrayEquals(testData, decompressed);
    }
}
